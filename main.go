package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/gotk3/gotk3/gtk"
)

type Action struct {
	key      string
	fileTime time.Time
	name     string
	exec     string
	mnemonic string
	icon     string
	score    int
	lastTime time.Time
	used     bool
}

func get_desktop_env() (desktop string) {

	kde0 := os.Getenv("KDE_SESSION_VERSION")
	kde1 := os.Getenv("KDE_FULL_VERSION")
	gnome := os.Getenv("GNOME_DESKTOP_SESSION_ID")
	session := os.Getenv("DESKTOP_SESSION")
	current_desktop := os.Getenv("XDG_CURRENT_DESKTOP")
	xdg_prefix := os.Getenv("XDG_MENU_PREFIX")

	desktop = "Old"

	if strings.Contains(session, "kde") || (kde0 != "") || (kde1 != "") {
		desktop = "KDE"
	} else if strings.Contains(session, "gnome") || (gnome != "") {
		desktop = "GNOME"
	} else if strings.Contains(session, "xfce") || (strings.Contains(xdg_prefix, "xfce")) {
		desktop = "XFCE"
	} else if strings.Contains(session, "lxde") || (strings.Contains(current_desktop, "lxde")) {
		desktop = "LXDE"
	} else if strings.Contains(session, "rox") {
		desktop = "ROX"
	} else if strings.Contains(session, "i3") {
		desktop = "I3"
	}

	return
}

func main() {

	oneWay := flag.Bool("one-way", true, "Indicates single action")

	flag.Parse()

	fmt.Println(oneWay)
	// Initialize GTK without parsing any command line arguments.
	gtk.Init(nil)

	label_value := get_desktop_env()
	// Create a new toplevel window, set its title, and connect it to the
	// "destroy" signal to exit the GTK main loop when it is destroyed.
	win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		log.Fatal("Unable to create window:", err)
	}
	win.SetTitle("Simple Example")
	win.Connect("destroy", func() {
		gtk.MainQuit()
	})

	// Create a new label widget to show in the window.
	l, err := gtk.LabelNew(label_value)
	if err != nil {
		log.Fatal("Unable to create label:", err)
	}

	// Add the label to the window.
	win.Add(l)

	// Set the default window size.
	win.SetDefaultSize(800, 600)

	// Recursively show all widgets contained in this window.
	win.ShowAll()

	// Begin executing the GTK main loop.  This blocks until
	// gtk.MainQuit() is run.
	gtk.Main()
}
